#ifndef COMPONENTS_DISPLACEMENT_H_
#define COMPONENTS_DISPLACEMENT_H_

namespace ECS {
	namespace components {
		struct Displacement {
			float x;
			float y;
		};
	}
}

#endif // !COMPONENTS_DISPLACEMENT_H_
