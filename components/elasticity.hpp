#ifndef COMPONENTS_ELASTICITY_H_
#define COMPONENTS_ELASTICITY_H_

namespace ECS {
	namespace components {
		struct Elasticity {
			float elasticity;
		};
	}
}

#endif // !COMPONENTS_ELASTICITY_H_
