#ifndef COMPONENTS_RENDER_H_
#define COMPONENTS_RENDER_H_

#include "utility/shader.hpp"

#include <GL/glew.h>

namespace ECS {
	namespace components {
		struct Render {
			Shader* shader;
			GLuint VAO;
			GLuint VBO;
			GLuint diffuseMap;
			GLuint specularMap;
		};
	}
}

#endif // !COMPONENTS_RENDER_H_
