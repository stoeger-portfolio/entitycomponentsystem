#ifndef COMPONENTS_VELOCITY_H_
#define COMPONENTS_VELOCITY_H_

namespace ECS {
	namespace components {
		struct Velocity {
			float x;
			float y;
		};
	}
}

#endif // !COMPONENTS_VELOCITY_H_
