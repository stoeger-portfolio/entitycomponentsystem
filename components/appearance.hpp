#ifndef COMPONENTS_APPEARANCE_H_
#define COMPONENTS_APPEARANCE_H_

namespace ECS {
	namespace components {
		struct Appearance {
			const char* name;
		};
	}
}

#endif // !COMPONENTS_APPEARANCE_H_
