#ifndef UTILITY_GLOBALS_H_
#define UTILITY_GLOBALS_H_

#include <stdint.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "utility/camera.hpp"

namespace ECS {
	namespace utility {
		static const GLint WIDTH = 800;
		static const GLint HEIGHT = 600;
		static const uint32_t ENTITY_COUNT = 20000;
		int SCREEN_WIDTH, SCREEN_HEIGHT; 
		GLFWwindow* window;
		Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

		bool firstMouse = true;
		bool keys[1024];
		GLfloat deltaTime = 0.0f;
		GLfloat lastFrame = 0.0f;
		GLfloat lastX = WIDTH / 2.0f;
		GLfloat lastY = WIDTH / 2.0f;

		static const int TICKS_PER_SECOND = 50;
		static const int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
		static const int MAX_FRAMESKIP = 10;
		int loops = 0;
	}
}

#endif // !UTILITY_GLOBALS_H_