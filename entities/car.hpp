#ifndef ENTITIES_CAR_H_
#define ENTITIES_CAR_H_

#include "utility/globals.hpp"
#include "components/component_mask.hpp"
#include "entities/world.hpp"
#include "utility/shader.hpp"

#include <stdint.h>
#include <stdio.h>

namespace ECS {
	namespace entities {
		namespace car {
			GLfloat vertices[] =
			{
				// Positions			// Normals			// TexCoords
				-0.5f, -0.5f, -0.5f,	0.0f, 0.0f, -1.0f,	0.0f, 0.0f,
				0.5f, -0.5f, -0.5f,		0.0f, 0.0f, -1.0f,	1.0f, 0.0f,
				0.5f,  0.5f, -0.5f,		0.0f, 0.0f, -1.0f,	1.0f, 1.0f,
				0.5f,  0.5f, -0.5f,		0.0f, 0.0f, -1.0f,	1.0f, 1.0f,
				-0.5f,  0.5f, -0.5f,	0.0f, 0.0f, -1.0f,	0.0f, 1.0f,
				-0.5f, -0.5f, -0.5f,	0.0f, 0.0f, -1.0f,	0.0f, 0.0f,

				-0.5f, -0.5f,  0.5f,	0.0f, 0.0f, 1.0f,	0.0f, 0.0f,
				0.5f, -0.5f,  0.5f,		0.0f, 0.0f, 1.0f,	1.0f, 0.0f,
				0.5f,  0.5f,  0.5f,		0.0f, 0.0f, 1.0f,	1.0f, 1.0f,
				0.5f,  0.5f,  0.5f,		0.0f, 0.0f, 1.0f,	1.0f, 1.0f,
				-0.5f,  0.5f,  0.5f,	0.0f, 0.0f, 1.0f,	0.0f, 1.0f,
				-0.5f, -0.5f,  0.5f,	0.0f, 0.0f, 1.0f,	0.0f, 0.0f,

				-0.5f,  0.5f,  0.5f,	-1.0f, 0.0f, 0.0f,	1.0f, 0.0f,
				-0.5f,  0.5f, -0.5f,	-1.0f, 0.0f, 0.0f,	1.0f, 1.0f,
				-0.5f, -0.5f, -0.5f,	-1.0f, 0.0f, 0.0f,	0.0f, 1.0f,
				-0.5f, -0.5f, -0.5f,	-1.0f, 0.0f, 0.0f,	0.0f, 1.0f,
				-0.5f, -0.5f,  0.5f,	-1.0f, 0.0f, 0.0f,	0.0f, 0.0f,
				-0.5f,  0.5f,  0.5f,	-1.0f, 0.0f, 0.0f,	1.0f, 0.0f,

				0.5f,  0.5f,  0.5f,		1.0f, 0.0f, 0.0f,	1.0f, 0.0f,
				0.5f,  0.5f, -0.5f,		1.0f, 0.0f, 0.0f,	1.0f, 1.0f,
				0.5f, -0.5f, -0.5f,		1.0f, 0.0f, 0.0f,	0.0f, 1.0f,
				0.5f, -0.5f, -0.5f,		1.0f, 0.0f, 0.0f,	0.0f, 1.0f,
				0.5f, -0.5f,  0.5f,		1.0f, 0.0f, 0.0f,	0.0f, 0.0f,
				0.5f,  0.5f,  0.5f,		1.0f, 0.0f, 0.0f,	1.0f, 0.0f,

				-0.5f, -0.5f, -0.5f,	0.0f, -1.0f, 0.0f,	0.0f, 1.0f,
				0.5f, -0.5f, -0.5f,		0.0f, -1.0f, 0.0f,	1.0f, 1.0f,
				0.5f, -0.5f,  0.5f,		0.0f, -1.0f, 0.0f,	1.0f, 0.0f,
				0.5f, -0.5f,  0.5f,		0.0f, -1.0f, 0.0f,	1.0f, 0.0f,
				-0.5f, -0.5f,  0.5f,	0.0f, -1.0f, 0.0f,	0.0f, 0.0f,
				-0.5f, -0.5f, -0.5f,	0.0f, -1.0f, 0.0f,	0.0f, 1.0f,

				-0.5f,  0.5f, -0.5f,	0.0f, 1.0f, 0.0f,	0.0f, 1.0f,
				0.5f,  0.5f, -0.5f,		0.0f, 1.0f, 0.0f,	1.0f, 1.0f,
				0.5f,  0.5f,  0.5f,		0.0f, 1.0f, 0.0f,	1.0f, 0.0f,
				0.5f,  0.5f,  0.5f,		0.0f, 1.0f, 0.0f,	1.0f, 0.0f,
				-0.5f,  0.5f,  0.5f,	0.0f, 1.0f, 0.0f,	0.0f, 0.0f,
				-0.5f,  0.5f, -0.5f,	0.0f, 1.0f, 0.0f, 	0.0f, 1.0f
			};

			GLuint diffuseMap = 0;
			GLuint specularMap = 0;

			static uint32_t CreateCar(World* world, float x, float y, Shader* shader)
			{
				uint32_t entity = world->createEntity();
				if (entity == ECS::utility::ENTITY_COUNT) {
					return entity;
				}

				world->mask[entity] = components::COMPONENT_DISPLACEMENT
					| components::COMPONENT_APPEARANCE
					| components::COMPONENT_VELOCITY
					| components::COMPONENT_GRAVITY
					| components::COMPONENT_RENDER;

				world->displacement[entity].x = x;
				world->displacement[entity].y = y;

				world->apperance[entity].name = "Car";

				world->velocity[entity].x = 0.0f;
				world->velocity[entity].y = 0.0f;

				world->elasticity[entity].elasticity = 0.8f;

				world->render[entity].shader = shader;

				glGenVertexArrays(1, &world->render[entity].VAO);
				glGenBuffers(1, &world->render[entity].VBO);

				glBindVertexArray(world->render[entity].VAO);

				glBindBuffer(GL_ARRAY_BUFFER, world->render[entity].VBO);
				glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

				// position attribute
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
				glEnableVertexAttribArray(0);

				// normal attribute
				glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
				glEnableVertexAttribArray(1);

				// texture attribute
				glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
				glEnableVertexAttribArray(2);

				glBindVertexArray(0); // Unbind VAO

				if (diffuseMap != 0 && specularMap != 0)
				{
					// Textures already loaded
					world->render[entity].diffuseMap = diffuseMap;
					world->render[entity].specularMap = specularMap;

					return entity;
				}

				glGenTextures(1, &world->render[entity].diffuseMap);
				glGenTextures(1, &world->render[entity].specularMap);

				diffuseMap = world->render[entity].diffuseMap;
				specularMap = world->render[entity].specularMap;

				int textureWidth, textureHeight;
				unsigned char* image;

				// diffuse map
				image = SOIL_load_image("resources/images/container2.png", &textureWidth, &textureHeight, 0, SOIL_LOAD_RGB);
				glBindTexture(GL_TEXTURE_2D, world->render[entity].diffuseMap);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
				glGenerateMipmap(GL_TEXTURE_2D);
				SOIL_free_image_data(image);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_LINEAR);

				//  specular map
				image = SOIL_load_image("resources/images/container2_specular.png", &textureWidth, &textureHeight, 0, SOIL_LOAD_RGB);
				glBindTexture(GL_TEXTURE_2D, world->render[entity].specularMap);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
				glGenerateMipmap(GL_TEXTURE_2D);
				SOIL_free_image_data(image);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_LINEAR);

				glBindTexture(GL_TEXTURE_2D, 0);

				world->render[entity].shader->Use();
				glUniform1i(glGetUniformLocation(world->render[entity].shader->Program, "material.diffuse"), 0);
				glUniform1i(glGetUniformLocation(world->render[entity].shader->Program, "material.specular"), 1);

				return entity;
			}
		}
	}
}
#endif // !ENTITIES_CAR_H_
