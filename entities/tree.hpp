#ifndef ENTITIES_TREE_H_
#define ENTITIES_TREE_H_

#include "utility/globals.hpp"
#include "components/component_mask.hpp"
#include "entities/world.hpp"

#include <stdint.h>
#include <stdio.h>

namespace ECS {
	namespace entities {
		static uint32_t CreateTree(World *world, float x, float y)
		{
			uint32_t entity = world->createEntity();
			if (entity == ECS::utility::ENTITY_COUNT) {
				return entity;
			}

			world->mask[entity] = components::COMPONENT_DISPLACEMENT | components::COMPONENT_APPEARANCE;

			world->displacement[entity].x = x;
			world->displacement[entity].y = y;

			world->apperance[entity].name = "Tree";

			return entity;
		}
	}
}

#endif // !ENTITIES_TREE_H_
