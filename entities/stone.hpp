#ifndef ENTITIES_STONE_H_
#define ENTITIES_STONE_H_

#include "utility/globals.hpp"
#include "components/component_mask.hpp"
#include "entities/world.hpp"

#include <stdint.h>
#include <stdio.h>

namespace ECS {
	namespace entities {
		static uint32_t CreateStone(World* world, float x, float y)
		{
			uint32_t entity = world->createEntity();
			if (entity == ECS::utility::ENTITY_COUNT) {
				return entity;
			}

			world->mask[entity] = components::COMPONENT_DISPLACEMENT | components::COMPONENT_APPEARANCE | components::COMPONENT_VELOCITY | components::COMPONENT_GRAVITY;

			world->displacement[entity].x = x;
			world->displacement[entity].y = y;

			world->apperance[entity].name = "Stone";

			world->velocity[entity].x = 0.0f;
			world->velocity[entity].y = 0.0f;

			world->elasticity[entity].elasticity = 0.8f;

			return entity;
		}
	}
}

#endif // !ENTITIES_STONE_H_
