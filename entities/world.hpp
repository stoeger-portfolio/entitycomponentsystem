#ifndef ENTITIES_WORLD_H_
#define ENTITIES_WORLD_H_

#include "utility/globals.hpp"
#include "components/appearance.hpp"
#include "components/displacement.hpp"
#include "components/velocity.hpp"
#include "components/elasticity.hpp"
#include "components/render.hpp"
#include "components/component_mask.hpp"

#include <stdint.h>
#include <stdio.h>

namespace ECS {
	namespace entities {
		class World {
		public:
			inline uint32_t createEntity() const
			{
				for (uint32_t entity = 0; entity < utility::ENTITY_COUNT; ++entity)
				{
					if (mask[entity] == components::COMPONENT_NONE)
					{
						return (entity);
					}
				}

				printf("ECS::ENTITIES::WORLD - Error! No more entities left!\n");
				return utility::ENTITY_COUNT;
			}

			inline void destroyEntity(const uint32_t entity)
			{
				mask[entity] = components::COMPONENT_NONE;
			}

			components::Displacement displacement[utility::ENTITY_COUNT];
			components::Appearance apperance[utility::ENTITY_COUNT];
			components::Velocity velocity[utility::ENTITY_COUNT];
			components::Elasticity elasticity[utility::ENTITY_COUNT];
			components::Render render[utility::ENTITY_COUNT];
			uint32_t mask[utility::ENTITY_COUNT];
		};
	}
}

#endif // !ENTITIES_WORLD_H_
