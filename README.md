In this project, the basics of entity component system (ECS) are tested.
A ball is affected by gravity and bounces back into the air.
A tree is not affected by velocity and gravity at all and has a fixed position.
The car travels with a constant velocity into one direction.
The stone is affected by gravity and drops to the ground without bouncing.

Entities:
Tree  (Appearance, Displacement)
Ball  (Appearance, Displacement, Velocity, Gravity, Elasticity)
Car   (Appearance, Displacement, Velocity, Gravity)
Stone (Appearance, Displacement, Velocity, Gravity)

Components:
Appearance (The name of the entity)
Displacement (The x and y coordinates of the entity)
Velocity (The x and y velocity of the entity)
Elasticity (The elasticity constant of the entity)

Systems:
Bounce    (Displacement, Velocity, Elasticity): Lets an entity bounce back into the air
Movement  (Displacement, Velocity): Moves an entity according to its velocity
Gravity   (Displacement, Velocity): Adds the gravity to an entities velocity
Apperance (Apperance, Displacement): Prints the entities name and current position