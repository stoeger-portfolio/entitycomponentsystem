#ifndef SYSTEMS_BOUNCE_H_
#define SYSTEMS_BOUNCE_H_

#include "utility/globals.hpp"
#include "entities/world.hpp"
#include "components/component_mask.hpp"

#include <stdint.h>
#include <stdio.h>

namespace ECS {
	namespace systems {
		static const uint32_t BOUNCE_MASK = (components::COMPONENT_DISPLACEMENT | components::COMPONENT_VELOCITY | components::COMPONENT_ELASTICITY);

		static void BounceFunction(entities::World* world)
		{
			components::Displacement* d;
			components::Velocity* v;
			components::Elasticity* e;

			for (uint32_t entity = 0; entity < utility::ENTITY_COUNT; ++entity)
			{
				if ((world->mask[entity] & BOUNCE_MASK) == BOUNCE_MASK)
				{
					d = &(world->displacement[entity]);
					v = &(world->velocity[entity]);
					e = &(world->elasticity[entity]);

					if (d->y <= 0)
					{
						d->y = 0.0f;
						v->y = -(v->y * e->elasticity);
					}
				}
			}
		}
	}
}

#endif // !SYSTEMS_BOUNCE_H_
