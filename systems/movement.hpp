#ifndef SYSTEMS_MOVEMENT_H_
#define SYSTEMS_MOVEMENT_H_

#include "utility/globals.hpp"
#include "entities/world.hpp"
#include "components/displacement.hpp"
#include "components/velocity.hpp"
#include "components/component_mask.hpp"

#include <stdint.h>
#include <stdio.h>

namespace ECS {
	namespace systems {
		static const uint32_t MOVEMENT_MASK = (components::COMPONENT_DISPLACEMENT | components::COMPONENT_VELOCITY);

		static void MovementFunction(entities::World* world)
		{
			components::Displacement* d;
			components::Velocity* v;

			for (uint32_t entity = 0; entity < utility::ENTITY_COUNT; ++entity)
			{
				if ((world->mask[entity] & MOVEMENT_MASK) == MOVEMENT_MASK)
				{
					d = &(world->displacement[entity]);
					v = &(world->velocity[entity]);
					
					d->x += v->x;
					d->y += v->y;
				}
			}
		}
	}
}

#endif // !SYSTEMS_MOVEMENT_H_
