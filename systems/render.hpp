#ifndef SYSTEMS_RENDER_H_
#define SYSTEMS_RENDER_H_

#include "utility/globals.hpp"
#include "entities/world.hpp"
#include "components/component_mask.hpp"

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stdint.h>
#include <stdio.h>

namespace ECS {
	namespace systems {
		static const uint32_t RENDER_MASK = (components::COMPONENT_APPEARANCE | components::COMPONENT_DISPLACEMENT | components::COMPONENT_RENDER);

		static void RenderFunction(entities::World* world)
		{
			components::Displacement* d;

			glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glm::mat4 projection(1.0f);
			projection = glm::perspective(ECS::utility::camera.GetZoom(), (GLfloat)ECS::utility::SCREEN_WIDTH / (GLfloat)ECS::utility::SCREEN_HEIGHT, 0.1f, 1000.0f);
			glm::mat4 view(1.0f);					
			view = ECS::utility::camera.GetViewMatrix();
			glm::mat4 model;					

			GLint lightDirLoc;
			GLint viewPosLoc;
			GLint modelLoc;
			GLint viewLoc;
			GLint projectionLoc;

			for (uint32_t entity = 0; entity < utility::ENTITY_COUNT; ++entity)
			{
				if ((world->mask[entity] & RENDER_MASK) == RENDER_MASK)
				{
					world->render[entity].shader->Use();
					lightDirLoc = glGetUniformLocation(world->render[entity].shader->Program, "light.direction");
					viewPosLoc = glGetUniformLocation(world->render[entity].shader->Program, "viewPos");
					modelLoc = glGetUniformLocation(world->render[entity].shader->Program, "model");
					viewLoc = glGetUniformLocation(world->render[entity].shader->Program, "view");
					projectionLoc =	glGetUniformLocation(world->render[entity].shader->Program, "projection");

					glUniform3f(lightDirLoc, -0.2f, -1.0f, -0.3f);
					glUniform3f(viewPosLoc, ECS::utility::camera.GetPosition().x, ECS::utility::camera.GetPosition().y, ECS::utility::camera.GetPosition().z);

					glUniform3f(glGetUniformLocation(world->render[entity].shader->Program, "light.ambient"), 0.2f, 0.2f, 0.2f);
					glUniform3f(glGetUniformLocation(world->render[entity].shader->Program, "light.diffuse"), 0.5f, 0.5f, 0.5f);
					glUniform3f(glGetUniformLocation(world->render[entity].shader->Program, "light.specular"), 1.0f, 1.0f, 1.0f);

					glUniform1f(glGetUniformLocation(world->render[entity].shader->Program, "material.shininess"), 32.0f);
					
					glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
					glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, world->render[entity].diffuseMap);

					glActiveTexture(GL_TEXTURE1);
					glBindTexture(GL_TEXTURE_2D, world->render[entity].specularMap);

					glBindVertexArray(world->render[entity].VAO);

					model = glm::mat4(1.0f);
					model = glm::translate(model, glm::vec3(world->displacement[entity].x, world->displacement[entity].y, 0.0f));

					glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
					glDrawArrays(GL_TRIANGLES, 0, 36);
				}

				glBindVertexArray(0);
			}
			glfwSwapBuffers(ECS::utility::window);
		}
	}
}

#endif // !SYSTEMS_RENDER_H_
