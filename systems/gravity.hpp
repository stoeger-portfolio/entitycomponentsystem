#ifndef SYSTEMS_GRAVITY_H_
#define SYSTEMS_GRAVITY_H_

#include "utility/globals.hpp"
#include "entities/world.hpp"
#include "components/displacement.hpp"
#include "components/velocity.hpp"
#include "components/component_mask.hpp"

#include <stdint.h>
#include <stdio.h>

namespace ECS {
	namespace systems {
		static const uint32_t GRAVITY_MASK = (components::COMPONENT_GRAVITY | components::COMPONENT_VELOCITY);

		static void GravityFunction(entities::World* world)
		{
			components::Displacement* d;
			components::Velocity* v;

			for (uint32_t entity = 0; entity < utility::ENTITY_COUNT; ++entity)
			{
				if ((world->mask[entity] & GRAVITY_MASK) == GRAVITY_MASK)
				{
					d = &(world->displacement[entity]);
					v = &(world->velocity[entity]);

					v->y -= 0.98f * 0.01f;

					d->y += v->y;

					if (d->y <= 0.1f)
					{
						d->y = 0.0f;
					}
				}
			}
		}
	}
}

#endif // !SYSTEMS_GRAVITY_H_
