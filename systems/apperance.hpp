#ifndef SYSTEMS_APPERANCE_H_
#define SYSTEMS_APPERANCE_H_

#include "utility/globals.hpp"
#include "entities/world.hpp"
#include "components/component_mask.hpp"

#include <stdint.h>
#include <stdio.h>

namespace ECS {
	namespace systems {
		static const uint32_t APPERANCE_MASK = (components::COMPONENT_APPEARANCE | components::COMPONENT_DISPLACEMENT);

		static void ApperanceFunction(entities::World* world)
		{
			components::Displacement* d;

			for (uint32_t entity = 0; entity < utility::ENTITY_COUNT; ++entity)
			{
				if ((world->mask[entity] & APPERANCE_MASK) == APPERANCE_MASK)
				{
					d = &(world->displacement[entity]);

					printf("ECS::Systems::ApperanceFunction: Entity '%s' position-x: '%f' position-y: '%f'\n", world->apperance[entity].name, d->x, d->y);
				}
			}
		}
	}
}

#endif // !SYSTEMS_APPERANCE_H_
