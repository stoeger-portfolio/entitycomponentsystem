#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "utility/globals.hpp"
#include "utility/camera.hpp"
#include "entities/world.hpp"
#include "entities/ball.hpp"
#include "entities/car.hpp"
#include "entities/tree.hpp"
#include "entities/stone.hpp"
#include "systems/gravity.hpp"
#include "systems/bounce.hpp"
#include "systems/movement.hpp"
#include "systems/apperance.hpp"
#include "systems/render.hpp"

#include <stdio.h>

void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);
void ScrollCallback(GLFWwindow* window, double xOffset, double yOffset);
void MouseCallback(GLFWwindow* window, double xPos, double yPos);
void DoMovement();

int main(void)
{
	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	ECS::utility::window = glfwCreateWindow(ECS::utility::WIDTH, ECS::utility::HEIGHT, "ECS", nullptr, nullptr);

	glfwGetFramebufferSize(ECS::utility::window, &ECS::utility::SCREEN_WIDTH, &ECS::utility::SCREEN_HEIGHT);

	if (ECS::utility::window == nullptr)
	{
		printf("Failed to create GLFW window\n");
		glfwTerminate();

		return 1;
	}

	glfwMakeContextCurrent(ECS::utility::window);

	glfwSetKeyCallback(ECS::utility::window, KeyCallback);
	glfwSetCursorPosCallback(ECS::utility::window, MouseCallback);
	glfwSetScrollCallback(ECS::utility::window, ScrollCallback);
	glfwSetInputMode(ECS::utility::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		printf("Failed to initialize GLEW\n");

		return 1;
	}

	glViewport(0, 0, ECS::utility::SCREEN_WIDTH, ECS::utility::SCREEN_HEIGHT);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);

	printf("Number of possible entities: %d\n", ECS::utility::ENTITY_COUNT);

	ECS::entities::World* world = new ECS::entities::World();

	Shader* lightningShader = new Shader("resources/shaders/lighting.vert", "resources/shaders/lighting.frag");
	
	uint32_t ball = ECS::entities::ball::CreateBall(world, 0.0f, 10.0f, lightningShader);
	uint32_t car = ECS::entities::car::CreateCar(world, 0.0f, 0.0f, lightningShader);
	uint32_t tree = ECS::entities::CreateTree(world, 0.0f, 100.0f);
	uint32_t stone = ECS::entities::CreateStone(world, 0.0f, 100.0f);
	
	world->velocity[car].x = 0.1f;

	for (size_t i = 0; i < ECS::utility::ENTITY_COUNT - 4; ++i)
	{
		ECS::entities::ball::CreateBall(world, i * 2, 10.0f + i, lightningShader);
	}
	
	GLfloat currentFrame;

	GLfloat nextGameTick = (GLfloat)glfwGetTime() * 1000;

	while (!glfwWindowShouldClose(ECS::utility::window)) {

		ECS::utility::loops = 0;
		while (glfwGetTime() * 1000 > nextGameTick && ECS::utility::loops < ECS::utility::MAX_FRAMESKIP)
		{
			currentFrame = (GLfloat)glfwGetTime();
			ECS::utility::deltaTime += currentFrame - ECS::utility::lastFrame;
			ECS::utility::lastFrame = currentFrame;

			glfwPollEvents();
			DoMovement();

			ECS::systems::MovementFunction(world);
			ECS::systems::GravityFunction(world);
			ECS::systems::BounceFunction(world);
			//ECS::systems::ApperanceFunction(world);

			nextGameTick += ECS::utility::SKIP_TICKS;
			++ECS::utility::loops;
		}
		ECS::systems::RenderFunction(world);
	}

	delete world;
	delete lightningShader;

	return 0;
}

void DoMovement()
{
	if (ECS::utility::keys[GLFW_KEY_W] || ECS::utility::keys[GLFW_KEY_UP])
	{
		ECS::utility::camera.ProcessKeyboard(FORWARD, ECS::utility::deltaTime);
	}

	if (ECS::utility::keys[GLFW_KEY_S] || ECS::utility::keys[GLFW_KEY_DOWN])
	{
		ECS::utility::camera.ProcessKeyboard(BACKWARD, ECS::utility::deltaTime);
	}

	if (ECS::utility::keys[GLFW_KEY_A] || ECS::utility::keys[GLFW_KEY_LEFT])
	{
		ECS::utility::camera.ProcessKeyboard(LEFT, ECS::utility::deltaTime);
	}

	if (ECS::utility::keys[GLFW_KEY_D] || ECS::utility::keys[GLFW_KEY_RIGHT])
	{
		ECS::utility::camera.ProcessKeyboard(RIGHT, ECS::utility::deltaTime);
	}
}

void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
		{
			ECS::utility::keys[key] = true;
		}
		else if (action == GLFW_RELEASE)
		{
			ECS::utility::keys[key] = false;
		}
	}

}

void MouseCallback(GLFWwindow* window, double xPos, double yPos)
{
	if (ECS::utility::firstMouse)
	{
		ECS::utility::lastX = xPos;
		ECS::utility::lastY = yPos;
		ECS::utility::firstMouse = false;
	}

	GLfloat xOffset = xPos - ECS::utility::lastX;
	GLfloat yOffset = ECS::utility::lastY - yPos;

	ECS::utility::lastX = xPos;
	ECS::utility::lastY = yPos;

	ECS::utility::camera.ProcessMouseMovement(xOffset, yOffset);
}

void ScrollCallback(GLFWwindow* window, double xOffset, double yOffset)
{
	ECS::utility::camera.ProcessMouseScroll(yOffset);
}